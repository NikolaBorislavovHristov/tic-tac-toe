# Build (you can skip this and use the [precompiled jar](https://github.com/NikolaBorislavovHristov/tic-tac-toe/releases/download/v1.0.0/tic-tac-toe-release.jar)) on linux

## Download the source code:
### If you have git installed:
```bash
git clone https://github.com/NikolaBorislavovHristov/tic-tac-toe.git && cd tic-tac-toe
```

### If you don't have git you can use the 'Download' button then unzip it and open a terminal in the directory of the source code

## Build a jar with dependencies:
```bash
./gradlew release
```

# Run

## Run the server:
```bash
java -cp build/libs/tic-tac-toe-release.jar com.stellarscript.tictactoe.server.ServerEntryPoint 13750
```

## Run the clients:
```bash
java -cp build/libs/tic-tac-toe-release.jar com.stellarscript.tictactoe.client.ClientEntryPoint 13750
```
