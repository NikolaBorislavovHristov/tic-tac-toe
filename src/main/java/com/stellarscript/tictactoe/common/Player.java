package com.stellarscript.tictactoe.common;

import java.io.Serializable;
import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Player extends Remote {

    String username() throws RemoteException;

    String password() throws RemoteException;

    Score score() throws RemoteException;

    interface Score extends Serializable {

        int wins();

        int losses();

        int draws();

    }

}
