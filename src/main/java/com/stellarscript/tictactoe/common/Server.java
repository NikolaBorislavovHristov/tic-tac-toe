package com.stellarscript.tictactoe.common;

import javax.naming.AuthenticationException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Map;

public interface Server extends Remote {

    Game[] games() throws RemoteException;

    Map<String, Player.Score> scoreboard() throws RemoteException;

    Player login(String username, String password) throws RemoteException, AuthenticationException;

    Game createGame(Player player) throws RemoteException, AuthenticationException;

}
