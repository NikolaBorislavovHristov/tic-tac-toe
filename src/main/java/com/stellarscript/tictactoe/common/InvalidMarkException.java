package com.stellarscript.tictactoe.common;

public final class InvalidMarkException extends Exception {

    public InvalidMarkException() {
        super();
    }

    public InvalidMarkException(final String message) {
        super(message);
    }

}
