package com.stellarscript.tictactoe.common;

public final class GameOverflowException extends Exception {

    public GameOverflowException() {
        super();
    }

    public GameOverflowException(final String message) {
        super(message);
    }

}
