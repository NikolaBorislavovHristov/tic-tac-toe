package com.stellarscript.tictactoe.common;

import javax.naming.AuthenticationException;
import java.io.Serializable;
import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Game extends Remote {

    void join(Player player) throws RemoteException, GameOverflowException, AuthenticationException;

    void mark(Player player, int row, int column) throws RemoteException, AuthenticationException, InvalidMarkException;

    int requiredPlayersCount() throws RemoteException;

    Status status() throws RemoteException;

    void addOnStatusChangeListener(OnStatusChangeListener listener) throws RemoteException;

    void removeOnStatusChangeListener(OnStatusChangeListener listener) throws RemoteException;

    interface OnStatusChangeListener extends Remote {

        void onStatusChange() throws RemoteException;

    }

    interface Status extends Serializable {

        String[][] field();

        String[] playerNames();

        boolean isFinished();

        boolean isStarted();

        String turn();

        String winner();

    }

}
