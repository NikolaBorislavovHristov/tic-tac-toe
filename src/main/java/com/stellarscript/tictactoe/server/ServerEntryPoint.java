package com.stellarscript.tictactoe.server;

import com.stellarscript.tictactoe.common.Server;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.text.MessageFormat;

public final class ServerEntryPoint {

    private static final String DATA_LOCATION = "data.json";

    public static void main(final String[] args) {
        if (args.length != 1) {
            throw new IllegalArgumentException("First argument expected to be the RMI registry port number");
        }

        try {
            final int port = Integer.parseInt(args[0]);
            final DbContext dbContext = new DbContext(DATA_LOCATION);
            final Server server = new RemoteServer(dbContext);
            final Registry registry = LocateRegistry.createRegistry(port);
            registry.rebind("server", server);
            System.out.println(MessageFormat.format("RMI Server is listening on port {0}", port));
        } catch (final Exception e) {
            e.printStackTrace();
        }
    }

}
