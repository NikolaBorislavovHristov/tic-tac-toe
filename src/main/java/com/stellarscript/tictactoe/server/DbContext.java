package com.stellarscript.tictactoe.server;

import org.apache.commons.io.FileUtils;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

final class DbContext {

    private static final Charset CHARSET = Charset.defaultCharset();
    private static final String USERS_KEY = "users";
    private static final String USERNAME_KEY = "username";
    private static final String PASSWORD_KEY = "password";
    private static final String SCORE_KEY = "score";

    static final String WINS_KEY = "wins";
    static final String LOSSES_KEY = "losses";
    static final String DRAWS_KEY = "draws";

    private final String dataLocation;

    DbContext(final String dataLocation) throws IOException {
        this.dataLocation = dataLocation;

        final File data = new File(this.dataLocation);
        if (!data.exists()) {
            FileUtils.touch(data);
            final JSONObject initialData = new JSONObject();
            initialData.put(USERS_KEY, new JSONObject());
            final String initialDataAsString = initialData.toString();
            FileUtils.writeStringToFile(data, initialDataAsString, CHARSET);
        }
    }

    public synchronized boolean areUserCredentialsMatch(final String username, final String password) {
        try {
            final File data = new File(this.dataLocation);
            final String databaseContentAsString = FileUtils.readFileToString(data, CHARSET);
            final JSONObject databaseContent = new JSONObject(databaseContentAsString);
            final JSONObject users = databaseContent.getJSONObject(USERS_KEY);
            if (!users.has(username)) {
                final JSONObject newUser = new JSONObject();
                newUser.put(USERNAME_KEY, username);
                newUser.put(PASSWORD_KEY, password);
                final JSONObject score = new JSONObject();
                score.put(WINS_KEY, 0);
                score.put(LOSSES_KEY, 0);
                score.put(DRAWS_KEY, 0);
                newUser.put(SCORE_KEY, score);
                users.put(username, newUser);
                final String newDatabaseContentAsString = databaseContent.toString();
                FileUtils.write(data, newDatabaseContentAsString, CHARSET);
                return true;
            }

            final JSONObject user = users.getJSONObject(username);
            return user.getString(PASSWORD_KEY).equals(password);
        } catch (final IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public synchronized Map<String, Integer> getScore(final String username) {
        try {
            final File data = new File(this.dataLocation);
            final String databaseContentAsString = FileUtils.readFileToString(data, CHARSET);
            final JSONObject databaseContent = new JSONObject(databaseContentAsString);
            final JSONObject users = databaseContent.getJSONObject(USERS_KEY);
            final JSONObject user = users.getJSONObject(username);
            final JSONObject score = user.getJSONObject(SCORE_KEY);
            return new HashMap<String, Integer>() {{
                put(WINS_KEY, score.getInt(WINS_KEY));
                put(LOSSES_KEY, score.getInt(LOSSES_KEY));
                put(DRAWS_KEY, score.getInt(DRAWS_KEY));
            }};
        } catch (final IOException e) {
            e.printStackTrace();
            return new HashMap<String, Integer>() {{
                put(WINS_KEY, 0);
                put(LOSSES_KEY, 0);
                put(DRAWS_KEY, 0);
            }};
        }
    }

    public synchronized void updateScore(final String username, final String scoreType) {
        try {
            final File data = new File(this.dataLocation);
            final String databaseContentAsString = FileUtils.readFileToString(data, CHARSET);
            final JSONObject databaseContent = new JSONObject(databaseContentAsString);
            final JSONObject users = databaseContent.getJSONObject(USERS_KEY);
            final JSONObject user = users.getJSONObject(username);
            final JSONObject score = user.getJSONObject(SCORE_KEY);
            score.increment(scoreType);
            final String newDatabaseContentAsString = databaseContent.toString();
            FileUtils.write(data, newDatabaseContentAsString, CHARSET);
        } catch (final IOException e) {
            e.printStackTrace();
        }
    }

    public synchronized Map<String, Map<String, Integer>> getScoreboard() {
        final HashMap<String, Map<String, Integer>> result = new HashMap<>();
        try {
            final File data = new File(this.dataLocation);
            final String databaseContentAsString = FileUtils.readFileToString(data, CHARSET);
            final JSONObject databaseContent = new JSONObject(databaseContentAsString);
            final JSONObject users = databaseContent.getJSONObject(USERS_KEY);
            final Set<String> usernames = users.keySet();
            for (final String username : usernames) {
                final JSONObject user = users.getJSONObject(username);
                final JSONObject score = user.getJSONObject(SCORE_KEY);
                result.put(username, new HashMap<String, Integer>() {{
                    put(WINS_KEY, score.getInt(WINS_KEY));
                    put(LOSSES_KEY, score.getInt(LOSSES_KEY));
                    put(DRAWS_KEY, score.getInt(DRAWS_KEY));
                }});
            }
        } catch (final IOException e) {
            e.printStackTrace();
        }

        return result;
    }

}
