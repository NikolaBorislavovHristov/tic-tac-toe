package com.stellarscript.tictactoe.server;

import com.stellarscript.tictactoe.common.Player;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Map;

final class RemotePlayer extends UnicastRemoteObject implements Player {

    private final DbContext dbContext;
    private final String username;
    private final String password;

    RemotePlayer(final DbContext dbContext, final String username, final String password) throws RemoteException {
        super();
        this.dbContext = dbContext;
        this.username = username;
        this.password = password;
    }

    @Override
    public String username() throws RemoteException {
        return this.username;
    }

    @Override
    public String password() throws RemoteException {
        return this.password;
    }

    @Override
    public synchronized Score score() throws RemoteException {
        return new RemoteScore(this.dbContext.getScore(this.username));
    }

    static final class RemoteScore implements Score {

        private static final long serialVersionUID = 22L;

        private final int wins;
        private final int losses;
        private final int draws;

        RemoteScore(final Map<String, Integer> score) {
            super();
            this.wins = score.get(DbContext.WINS_KEY);
            this.losses = score.get(DbContext.LOSSES_KEY);
            this.draws = score.get(DbContext.DRAWS_KEY);
        }

        RemoteScore(final int wins, final int losses, final int draws) {
            super();
            this.wins = wins;
            this.losses = losses;
            this.draws = draws;
        }

        public int wins() {
            return this.wins;
        }

        public int losses() {
            return this.losses;
        }

        public int draws() {
            return this.draws;
        }

    }

}
