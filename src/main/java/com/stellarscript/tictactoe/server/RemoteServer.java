package com.stellarscript.tictactoe.server;

import com.stellarscript.tictactoe.common.Game;
import com.stellarscript.tictactoe.common.Player;
import com.stellarscript.tictactoe.common.Server;

import javax.naming.AuthenticationException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;

final class RemoteServer extends UnicastRemoteObject implements Server {

    private final ArrayList<Game> games;
    private final DbContext dbContext;

    RemoteServer(final DbContext dbContext) throws RemoteException {
        super();
        this.games = new ArrayList<>();
        this.dbContext = dbContext;
    }

    @Override
    public synchronized Game[] games() throws RemoteException {
        this.games.removeIf(new Predicate<Game>() {
            @Override
            public boolean test(final Game game) {
                try {
                    return game.status().isFinished();
                } catch (final RemoteException e) {
                    e.printStackTrace();
                }

                return false;
            }
        });

        return this.games.toArray(new Game[this.games.size()]);
    }

    @Override
    public Map<String, Player.Score> scoreboard() throws RemoteException {
        final HashMap<String, Player.Score> result = new HashMap<>();
        final Set<Map.Entry<String, Map<String, Integer>>> scoreboardEntries = this.dbContext.getScoreboard().entrySet();
        for (final Map.Entry<String, Map<String, Integer>> entry : scoreboardEntries) {
            final String username = entry.getKey();
            final Map<String, Integer> score = entry.getValue();
            result.put(username, new RemotePlayer.RemoteScore(score));
        }

        return result;
    }

    @Override
    public Player login(final String username, final String password) throws RemoteException, AuthenticationException {
        if (username == null || username.isEmpty()) {
            throw new AuthenticationException("Invalid username");
        }

        if (password == null || password.isEmpty()) {
            throw new AuthenticationException("Invalid password");
        }

        if (!this.dbContext.areUserCredentialsMatch(username, password)) {
            throw new AuthenticationException("Invalid credentials");
        }

        return new RemotePlayer(this.dbContext, username, password);
    }

    @Override
    public synchronized Game createGame(final Player player) throws RemoteException, AuthenticationException {
        if (player == null) {
            throw new AuthenticationException("You are not authenticated.");
        }

        if (!this.dbContext.areUserCredentialsMatch(player.username(), player.password())) {
            throw new AuthenticationException("Invalid credentials");
        }

        final Game game = new RemoteGame(this.dbContext, player);
        this.games.add(game);
        return game;
    }

}
