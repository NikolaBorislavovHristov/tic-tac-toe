package com.stellarscript.tictactoe.server;

import com.stellarscript.tictactoe.common.Game;
import com.stellarscript.tictactoe.common.GameOverflowException;
import com.stellarscript.tictactoe.common.InvalidMarkException;
import com.stellarscript.tictactoe.common.Player;

import javax.naming.AuthenticationException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.HashSet;

final class RemoteGame extends UnicastRemoteObject implements Game {

    private static final int SAME_MARKS_COUNT_FOR_WIN = 3;

    private final Object lock;
    private final DbContext dbContext;
    private final HashSet<OnStatusChangeListener> statusChangeListeners;
    private final HashMap<String, Player> players;
    private final int requiredPlayersCount;
    private final String[][] field;

    RemoteGame(final DbContext dbContext, final Player host) throws RemoteException {
        this(2, 3, dbContext, host);
    }

    private RemoteGame(final int requiredPlayersCount, final int fieldSize, final DbContext dbContext, final Player host) throws RemoteException {
        super();
        this.lock = new Object();
        this.statusChangeListeners = new HashSet<>();
        this.players = new HashMap<>();
        this.requiredPlayersCount = requiredPlayersCount;
        this.field = new String[fieldSize][fieldSize];
        this.dbContext = dbContext;
        this.players.put(host.username(), host);
    }

    @Override
    public void join(final Player player) throws RemoteException, GameOverflowException, AuthenticationException {
        synchronized (this.lock) {
            if (player == null) {
                throw new AuthenticationException("You are not authenticated.");
            }

            if (!this.dbContext.areUserCredentialsMatch(player.username(), player.password())) {
                throw new AuthenticationException("You are not authenticated.");
            }

            if (this.players.containsKey(player.username())) {
                throw new AuthenticationException("You are already part of this game.");
            }

            if (this.players.size() == this.requiredPlayersCount) {
                throw new GameOverflowException("The game is full.");
            }

            this.players.put(player.username(), player);
        }

        this.onStatusChange();
    }

    @Override
    public void mark(final Player player, final int row, final int column) throws RemoteException, AuthenticationException, InvalidMarkException {
        synchronized (this.lock) {
            if (player == null) {
                throw new AuthenticationException("You are not authenticated.");
            }

            if (!this.dbContext.areUserCredentialsMatch(player.username(), player.password())) {
                throw new AuthenticationException("You are not authenticated.");
            }

            if (!this.players.containsKey(player.username())) {
                throw new AuthenticationException("You are not part of this game.");
            }

            final Status status = this.status();
            if (!status.isStarted()) {
                throw new InvalidMarkException("The game is not started.");
            }

            if (status.isFinished()) {
                throw new InvalidMarkException("The game is finished.");
            }

            if (!status.turn().equals(player.username())) {
                throw new InvalidMarkException("It is not your turn.");
            }

            if (column >= this.field.length || column < 0 || row >= this.field.length || row < 0) {
                throw new InvalidMarkException("The mark is out of field's bounds.");
            }

            if (this.field[row][column] != null) {
                throw new InvalidMarkException("The field is used by another player.");
            }

            this.field[row][column] = player.username();
        }

        this.onStatusChange();
    }

    @Override
    public int requiredPlayersCount() throws RemoteException {
        return this.requiredPlayersCount;
    }

    @Override
    public Status status() throws RemoteException {
        synchronized (this.lock) {
            final String[] playerNames = this.players.keySet().toArray(new String[this.players.size()]);
            final String winner = this.checkFieldForWinner();
            final int marksCount = this.marksCount();
            final boolean isDraw = winner == null && marksCount == Math.pow(this.field.length, 2);
            final boolean isFinished = isDraw || winner != null;
            final boolean isStarted = this.players.size() == this.requiredPlayersCount;
            final String turn;
            if (isStarted) {
                final int playerIndexTurn = marksCount % this.players.size();
                turn = (String) this.players.keySet().toArray()[playerIndexTurn];
            } else {
                turn = null;
            }

            return new RemoteStatus(this.field, playerNames, isFinished, isStarted, turn, winner);
        }
    }

    @Override
    public void addOnStatusChangeListener(final OnStatusChangeListener listener) throws RemoteException {
        synchronized (this.lock) {
            this.statusChangeListeners.add(listener);
        }
    }

    @Override
    public void removeOnStatusChangeListener(final OnStatusChangeListener listener) throws RemoteException {
        synchronized (this.lock) {
            this.statusChangeListeners.remove(listener);
        }
    }

    private void onStatusChange() throws RemoteException {
        for (final OnStatusChangeListener listener : this.statusChangeListeners) {
            listener.onStatusChange();
        }

        final Status status = this.status();
        if (status.isFinished()) {
            final String winner = status.winner();
            final String[] playerNames = status.playerNames();
            for (final String username : playerNames) {
                if (winner == null) {
                    this.dbContext.updateScore(username, DbContext.DRAWS_KEY);
                } else if (winner.equals(username)) {
                    this.dbContext.updateScore(username, DbContext.WINS_KEY);
                } else {
                    this.dbContext.updateScore(username, DbContext.LOSSES_KEY);
                }
            }
        }
    }

    private String checkFieldForWinner() {
        // horizontal
        for (final String[] row : this.field) {
            final String winner = this.checkLineForWinner(row);
            if (winner != null) {
                return winner;
            }
        }

        // vertical
        for (int columnIndex = 0; columnIndex < this.field.length; columnIndex++) {
            final String[] column = new String[this.field.length];
            for (int rowIndex = 0; rowIndex < this.field.length; rowIndex++) {
                final String mark = this.field[rowIndex][columnIndex];
                column[rowIndex] = mark;
            }

            final String winner = this.checkLineForWinner(column);
            if (winner != null) {
                return winner;
            }
        }

        // top-right
        for (int columnIndex = 0; columnIndex < this.field.length; columnIndex++) {
            final String[] diagonal = new String[this.field.length];
            for (int rowIndex = 0; rowIndex < this.field.length - columnIndex; rowIndex++) {
                final String mark = this.field[rowIndex][rowIndex + columnIndex];
                diagonal[rowIndex] = mark;
            }

            final String winner = this.checkLineForWinner(diagonal);
            if (winner != null) {
                return winner;
            }
        }

        // bottom-left
        for (int rowIndex = 0; rowIndex < this.field.length; rowIndex++) {
            final String[] diagonal = new String[this.field.length];
            for (int columnIndex = 0; columnIndex < this.field.length - rowIndex; columnIndex++) {
                final String mark = this.field[rowIndex + columnIndex][columnIndex];
                diagonal[columnIndex] = mark;
            }

            final String winner = this.checkLineForWinner(diagonal);
            if (winner != null) {
                return winner;
            }
        }

        // top-left
        for (int columnIndex = this.field.length - 1; columnIndex >= 0; columnIndex--) {
            final String[] diagonal = new String[this.field.length];
            for (int rowIndex = 0; rowIndex < columnIndex + 1; rowIndex++) {
                final String mark = this.field[rowIndex][columnIndex - rowIndex];
                diagonal[rowIndex] = mark;
            }

            final String winner = this.checkLineForWinner(diagonal);
            if (winner != null) {
                return winner;
            }
        }

        // bottom-right
        for (int rowIndex = 0; rowIndex < this.field.length; rowIndex++) {
            final String[] diagonal = new String[this.field.length];
            for (int columnIndex = this.field.length - 1; columnIndex >= rowIndex; columnIndex--) {
                final String mark = this.field[this.field.length - (columnIndex - rowIndex) - 1][columnIndex];
                diagonal[columnIndex] = mark;
            }

            final String winner = this.checkLineForWinner(diagonal);
            if (winner != null) {
                return winner;
            }
        }

        return null;
    }

    private String checkLineForWinner(final String[] line) {
        for (int i = 0, sameMarksCount = 1; i <= line.length - SAME_MARKS_COUNT_FOR_WIN + 1; i++) {
            final String mark = line[i];
            final String nextMark = line[i + 1];
            if (mark != null && mark.equals(nextMark)) {
                sameMarksCount++;
                if (sameMarksCount == SAME_MARKS_COUNT_FOR_WIN) {
                    return mark;
                }
            } else {
                sameMarksCount = 1;
            }
        }

        return null;
    }

    private int marksCount() {
        int marksCount = 0;
        for (final String[] row : this.field) {
            for (final String mark : row) {
                if (mark != null) {
                    marksCount++;
                }
            }
        }

        return marksCount;
    }

    static final class RemoteStatus implements Status {

        private static final long serialVersionUID = 21L;

        private final String[][] field;
        private final String[] playerNames;
        private final boolean isFinished;
        private final boolean isStarted;
        private final String turn;
        private final String winner;

        RemoteStatus(final String[][] field, final String[] playerNames, final boolean isFinished, final boolean isStarted, final String turn, final String winner) {
            super();
            this.field = field;
            this.playerNames = playerNames;
            this.isFinished = isFinished;
            this.isStarted = isStarted;
            this.turn = turn;
            this.winner = winner;
        }

        @Override
        public String[][] field() {
            return this.field;
        }

        @Override
        public String[] playerNames() {
            return this.playerNames;
        }

        @Override
        public boolean isFinished() {
            return this.isFinished;
        }

        @Override
        public boolean isStarted() {
            return this.isStarted;
        }

        @Override
        public String turn() {
            return this.turn;
        }

        @Override
        public String winner() {
            return this.winner;
        }

        @Override
        public String toString() {
            if (this.winner != null) {
                return "The winner is " + this.winner;
            }

            if (this.isFinished) {
                return "The result is draw";
            }

            if (!this.isStarted) {
                return "Waiting for opponent";
            }

            return "It is " + this.turn + "'s turn";
        }

    }

}
