package com.stellarscript.tictactoe.client;

import com.stellarscript.tictactoe.common.*;

import javax.naming.AuthenticationException;
import javax.swing.*;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.text.MessageFormat;
import java.util.Map.Entry;

public final class ClientEntryPoint {

    private final Server server;
    private Player player;
    private Game game;

    private ClientEntryPoint(final Server server) {
        this.server = server;
    }

    public static void main(final String[] args) {
        if (args.length != 1) {
            throw new IllegalArgumentException("First argument expected to be the RMI registry port number");
        }

        try {
            final int port = Integer.parseInt(args[0]);
            final Registry registry = LocateRegistry.getRegistry(port);
            final Server server = (Server) registry.lookup("server");
            final ClientEntryPoint client = new ClientEntryPoint(server);
            client.run();
        } catch (final RemoteException | NotBoundException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }

    private void run() throws RemoteException {
        final JFrame mainFrame = new JFrame("Tic Tac Toe");
        mainFrame.setSize(new Dimension(400, 440));
        mainFrame.setResizable(false);
        mainFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        final JPanel mainPanel = new JPanel();
        mainPanel.setSize(400, 400);
        mainPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 0, 0));

        final JPanel loginPanel = new JPanel();
        loginPanel.setSize(400, 400);
        loginPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 0, 0));

        final JPanel profilePanel = new JPanel();
        profilePanel.setSize(400, 400);
        profilePanel.setLayout(new FlowLayout(FlowLayout.CENTER, 0, 0));

        final JPanel scorePanel = new JPanel();
        scorePanel.setSize(400, 400);
        scorePanel.setLayout(new FlowLayout(FlowLayout.CENTER, 0, 0));

        final JPanel gamePanel = new JPanel();
        gamePanel.setSize(400, 400);
        gamePanel.setLayout(new FlowLayout(FlowLayout.CENTER, 0, 0));

        //Game panel
        final JButton backFromGame = new JButton("Back to main screen");
        backFromGame.setPreferredSize(new Dimension(400, 50));
        backFromGame.setVisible(false);
        backFromGame.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent ev) {
                mainFrame.getContentPane().removeAll();
                mainFrame.getContentPane().add(mainPanel);
                mainFrame.revalidate();
                mainFrame.repaint();
            }
        });
        final JLabel gameStatusLabel = new JLabel();
        gameStatusLabel.setPreferredSize(new Dimension(400, 50));
        final JPanel gameField = new JPanel();
        gameField.setPreferredSize(new Dimension(300, 300));
        gameField.setLayout(new GridLayout(3, 3, 0, 0));
        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                final JButton cell = new JButton();
                final int cellRow = row;
                final int cellCol = col;
                cell.setPreferredSize(new Dimension(50, 50));
                cell.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(final ActionEvent ev) {
                        try {
                            game.mark(player, cellRow, cellCol);
                        } catch (final RemoteException | AuthenticationException | InvalidMarkException e) {
                            e.printStackTrace();
                            JOptionPane.showMessageDialog(null, e.getMessage());
                        }
                    }
                });
                gameField.add(cell);
            }
        }
        final Game.OnStatusChangeListener gameStatusChangeListener = (Game.OnStatusChangeListener) UnicastRemoteObject.exportObject(new Game.OnStatusChangeListener() {
            @Override
            public void onStatusChange() throws RemoteException {
                final Game.Status status = game.status();
                gameStatusLabel.setText(status.toString());
                final String[][] field = status.field();
                for (int row = 0; row < 3; row++) {
                    for (int col = 0; col < 3; col++) {
                        final int componentIndex = (row * 3) + col;
                        final String username = field[row][col];
                        ((JButton) gameField.getComponent(componentIndex)).setText(username);
                    }
                }
                if (status.isFinished()) {
                    backFromGame.setVisible(true);
                }
            }
        }, 0);
        gamePanel.add(backFromGame);
        gamePanel.add(gameStatusLabel);
        gamePanel.add(gameField);
        gamePanel.addAncestorListener(new AncestorListener() {
            @Override
            public void ancestorAdded(final AncestorEvent ev) {
                gameStatusLabel.setText("");
                for (final Component cell : gameField.getComponents()) {
                    if (cell instanceof JButton) {
                        ((JButton) cell).setText("");
                    }
                }

                try {
                    game.addOnStatusChangeListener(gameStatusChangeListener);
                    gameStatusChangeListener.onStatusChange();
                } catch (final RemoteException e) {
                    e.printStackTrace();
                    JOptionPane.showMessageDialog(null, e.getMessage());
                }
            }

            @Override
            public void ancestorRemoved(final AncestorEvent ev) {
                try {
                    game.removeOnStatusChangeListener(gameStatusChangeListener);
                    backFromGame.setVisible(false);
                } catch (final RemoteException e) {
                    e.printStackTrace();
                    JOptionPane.showMessageDialog(null, e.getMessage());
                }
            }

            @Override
            public void ancestorMoved(final AncestorEvent ev) {
            }
        });

        //Score panel
        final JButton backFromScoreboard = new JButton("Back to main screen");
        backFromScoreboard.setPreferredSize(new Dimension(400, 60));
        backFromScoreboard.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent ev) {
                mainFrame.getContentPane().removeAll();
                mainFrame.getContentPane().add(mainPanel);
                mainFrame.revalidate();
                mainFrame.repaint();
            }
        });
        final JList<Object> scoresList = new JList<>();
        scoresList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        scoresList.setLayoutOrientation(JList.VERTICAL);
        scoresList.setCellRenderer(new ListCellRenderer<Object>() {
            @Override
            public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
                final JLabel label = new JLabel();
                label.setOpaque(true);
                final String key = ((Entry<String, Player.Score>) value).getKey();
                final Player.Score score = ((Entry<String, Player.Score>) value).getValue();
                label.setText(MessageFormat.format("{0} : {1} / {2} / {3}", key, score.wins(), score.draws(), score.losses()));
                if (isSelected) {
                    label.setBackground(Color.yellow);
                }

                return label;
            }
        });
        final JScrollPane scoresListPanel = new JScrollPane(scoresList);
        scoresListPanel.setPreferredSize(new Dimension(400, 280));
        final JButton scoreboardRefreshButton = new JButton("Refresh Scoreboard");
        scoreboardRefreshButton.setPreferredSize(new Dimension(400, 60));
        scoreboardRefreshButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent ev) {
                try {
                    scoresList.setListData(server.scoreboard().entrySet().toArray());
                } catch (final RemoteException e) {
                    e.printStackTrace();
                    JOptionPane.showMessageDialog(null, e.getMessage());
                }
            }
        });
        scorePanel.add(backFromScoreboard);
        scorePanel.add(scoreboardRefreshButton);
        scorePanel.add(scoresListPanel);
        scorePanel.addAncestorListener(new AncestorListener() {
            @Override
            public void ancestorAdded(final AncestorEvent ev) {
                scoreboardRefreshButton.doClick();
            }

            @Override
            public void ancestorRemoved(final AncestorEvent ev) {
            }

            @Override
            public void ancestorMoved(final AncestorEvent ev) {
            }
        });

        //Profile panel
        final JButton backFromProfile = new JButton("Back to main screen");
        backFromProfile.setPreferredSize(new Dimension(400, 60));
        backFromProfile.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent ev) {
                mainFrame.getContentPane().removeAll();
                mainFrame.getContentPane().add(mainPanel);
                mainFrame.revalidate();
                mainFrame.repaint();
            }
        });
        final JLabel playerUsername = new JLabel();
        playerUsername.setPreferredSize(new Dimension(400, 60));
        final JLabel playerScore = new JLabel();
        playerScore.setPreferredSize(new Dimension(400, 60));
        final JButton logoutButton = new JButton("Logout");
        logoutButton.setPreferredSize(new Dimension(400, 60));
        logoutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent ev) {
                player = null;
                backFromProfile.doClick();
            }
        });
        profilePanel.add(backFromProfile);
        profilePanel.add(playerUsername);
        profilePanel.add(playerScore);
        profilePanel.add(logoutButton);
        profilePanel.addAncestorListener(new AncestorListener() {
            @Override
            public void ancestorAdded(final AncestorEvent ev) {
                try {
                    final Player.Score score = player.score();
                    playerScore.setText(MessageFormat.format("Wins / Draws / Losses: {0} / {1} / {2}", score.wins(), score.draws(), score.losses()));
                    playerUsername.setText(player.username());
                } catch (final Exception e) {
                    e.printStackTrace();
                    JOptionPane.showMessageDialog(null, e.getMessage());
                }
            }

            @Override
            public void ancestorRemoved(final AncestorEvent ev) {
            }

            @Override
            public void ancestorMoved(final AncestorEvent ev) {
            }
        });

        // Login panel children
        final JButton backFromLogin = new JButton("Back to main screen");
        backFromLogin.setPreferredSize(new Dimension(400, 60));
        backFromLogin.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent ev) {
                mainFrame.getContentPane().removeAll();
                mainFrame.getContentPane().add(mainPanel);
                mainFrame.revalidate();
                mainFrame.repaint();
            }
        });
        final JLabel usernameLabel = new JLabel("Username: ");
        usernameLabel.setPreferredSize(new Dimension(200, 60));
        final JTextField usernameField = new JTextField();
        usernameField.setPreferredSize(new Dimension(200, 60));
        final JLabel passwordLabel = new JLabel("Password: ");
        passwordLabel.setPreferredSize(new Dimension(200, 60));
        final JPasswordField passwordField = new JPasswordField();
        passwordField.setEchoChar('*');
        passwordField.setPreferredSize(new Dimension(200, 60));
        final JButton loginButton = new JButton("Login");
        loginButton.setPreferredSize(new Dimension(400, 60));
        loginButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent ev) {
                try {
                    player = server.login(usernameField.getText(), String.valueOf(passwordField.getPassword()));
                    JOptionPane.showMessageDialog(null, "You are logged in!");
                    backFromLogin.doClick();
                } catch (final RemoteException | AuthenticationException e) {
                    e.printStackTrace();
                    JOptionPane.showMessageDialog(null, e.getMessage());
                }
            }
        });
        loginPanel.add(backFromLogin);
        loginPanel.add(usernameLabel);
        loginPanel.add(usernameField);
        loginPanel.add(passwordLabel);
        loginPanel.add(passwordField);
        loginPanel.add(loginButton);

        // Main panel children
        final JButton goToLoginButton = new JButton("Login");
        goToLoginButton.setPreferredSize(new Dimension(200, 60));
        goToLoginButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent ev) {
                mainFrame.getContentPane().removeAll();
                mainFrame.getContentPane().add(loginPanel);
                mainFrame.revalidate();
                mainFrame.repaint();
            }
        });
        final JButton goToProfileButton = new JButton("Profile");
        goToProfileButton.setPreferredSize(new Dimension(200, 60));
        goToProfileButton.setVisible(false);
        goToProfileButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent ev) {
                mainFrame.getContentPane().removeAll();
                mainFrame.getContentPane().add(profilePanel);
                mainFrame.revalidate();
                mainFrame.repaint();
            }
        });
        final JButton goToScoreboardButton = new JButton("Scoreboard");
        goToScoreboardButton.setPreferredSize(new Dimension(200, 60));
        goToScoreboardButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent ev) {
                mainFrame.getContentPane().removeAll();
                mainFrame.getContentPane().add(scorePanel);
                mainFrame.revalidate();
                mainFrame.repaint();
            }
        });
        final JList<Game> gamesList = new JList<>();
        gamesList.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        gamesList.setLayoutOrientation(JList.VERTICAL);
        gamesList.setCellRenderer(new ListCellRenderer<Game>() {
            @Override
            public Component getListCellRendererComponent(JList<? extends Game> list, Game value, int index, boolean isSelected, boolean cellHasFocus) {
                final JLabel label = new JLabel();
                label.setOpaque(true);
                try {
                    final Game.Status status = value.status();
                    label.setText(MessageFormat.format("{0} / {1} : {2}", status.playerNames().length, value.requiredPlayersCount(), String.join(", ", status.playerNames())));
                    if (isSelected) {
                        label.setBackground(Color.yellow);
                    }
                } catch (final RemoteException e) {
                    e.printStackTrace();
                    JOptionPane.showMessageDialog(null, e.getMessage());
                }

                return label;
            }
        });
        gamesList.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(final ListSelectionEvent ev) {
                if (!ev.getValueIsAdjusting()) {
                    try {
                        game = gamesList.getSelectedValue();
                        if (game != null) {
                            game.join(player);
                            mainFrame.getContentPane().removeAll();
                            mainFrame.getContentPane().add(gamePanel);
                            mainFrame.revalidate();
                            mainFrame.repaint();
                        }
                    } catch (final RemoteException | GameOverflowException | AuthenticationException e) {
                        e.printStackTrace();
                        JOptionPane.showMessageDialog(null, e.getMessage());
                    }
                }
            }
        });
        final JButton gameCreateButton = new JButton("Create Game");
        gameCreateButton.setPreferredSize(new Dimension(400, 60));
        gameCreateButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent ev) {
                try {
                    game = server.createGame(player);
                    mainFrame.getContentPane().removeAll();
                    mainFrame.getContentPane().add(gamePanel);
                    mainFrame.revalidate();
                    mainFrame.repaint();
                } catch (final RemoteException | AuthenticationException e) {
                    e.printStackTrace();
                    JOptionPane.showMessageDialog(null, e.getMessage());
                }
            }
        });
        final JButton gamesRefreshButton = new JButton("Refresh Games");
        gamesRefreshButton.setPreferredSize(new Dimension(400, 60));
        gamesRefreshButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent ev) {
                try {
                    gamesList.setListData(server.games());
                } catch (final RemoteException e) {
                    e.printStackTrace();
                    JOptionPane.showMessageDialog(null, e.getMessage());
                }
            }
        });
        final JScrollPane gamesListPanel = new JScrollPane(gamesList);
        gamesListPanel.setPreferredSize(new Dimension(400, 240));
        mainPanel.add(goToLoginButton);
        mainPanel.add(goToProfileButton);
        mainPanel.add(goToScoreboardButton);
        mainPanel.add(gameCreateButton);
        mainPanel.add(gamesRefreshButton);
        mainPanel.add(gamesListPanel);
        mainPanel.addAncestorListener(new AncestorListener() {
            @Override
            public void ancestorAdded(final AncestorEvent ev) {
                goToProfileButton.setVisible(player != null);
                goToLoginButton.setVisible(player == null);
                gamesRefreshButton.doClick();
            }

            @Override
            public void ancestorRemoved(final AncestorEvent ev) {
            }

            @Override
            public void ancestorMoved(final AncestorEvent ev) {
            }
        });

        mainFrame.add(mainPanel);
        mainFrame.setVisible(true);

        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                gamesRefreshButton.doClick();
            }
        });
    }
}
